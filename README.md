This is a template project for creating scientific manuscripts, designed to be used (by multiple co-authors) with Overleaf (www.overleaf.com).

The key idea is to separate the "editing" and the "formatting" (useful when your paper is not accepted and you want to try on another journal but you have to change the format...).

- text.tex: you should write your manuscript on this file. I put here some text to show how to include figures, create tables, refer to them and cite papers
- suppmat.tex : you will put all what cannot fit into your main paper here
- title\_XXX.tex: these files should take care of all the formatting. Each file should "render" the manuscript to the specific format required by different journals. These files also define the first-page/title with authors and affiliations, and abstract, keywords and all other stuff that is usually needed. You will "compile" only the title\_XXX.tex that you will need
- figures/ : you will put your images here
- styles/ : files needed by each style are saved into each specific sub-folder
- references.bib : file where to put your references


### Latex tables
Useful tool to create Latex Tables: https://www.tablesgenerator.com/
You can copy-paste a table from your favorite spreadsheet editor and convert it to Latex (File > Paste Table data). Some fine-tuning will be needed for "eye-candy" rendering.

### Bibtex references
I suggest to search the paper you want to cite on Google-scholar. Once found, clic on the " below the paper entry and click on bibtex. Copy the text that will be shown and paste it in the references.bib .

### Contribute
If you create a title_XXX.tex for a new style, please open a pull-request to have it included.

### Latex 2 docx
These are the steps I use to convert the manuscript into docx. After several trial-and-error I found this solution that works quite well. Of course, if you use complicated things, \\newcommands, strange packages and so on and so forth, you are on your own to make this working.
However, for standard manuscripts (I suggest to stick to the bare article class) the results are generally satisfactory.
You will need the basic TeX programs (latex, bibtex) and make4ht (https://github.com/michal-h21/make4ht).

1. If you are using Overleaf, download the project. Extract the downloaded archive and, if you are using customized classes or styles, remember to include all the needed style files.
2. Compile your project using latex:
$ latex title_article.tex
3. Use bibtex to solve the references:
$ bibtex title_article
4. Compile again to include the references:
$ latex title_article.tex
5. If everything worked smooth, it should be possible to convert the project to html:
$ make4ht title_article.tex

6. Open the created html file with your favorite .doc editor (e.g. MS Word or similar) and fix the minor issues
7. Save the "fixed" document as a .docx document


### Contact
andrea.bizzego@unitn.it